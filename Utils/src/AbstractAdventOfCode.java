import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.List;

public class AbstractAdventOfCode implements AdventOfCode {

    public final String NOT_CALCULATED = "Not calculated yet.";
    protected List<String> input;
    protected String firstPart;
    protected String secondPart;


    private AbstractAdventOfCode() {
    }

    public AbstractAdventOfCode(String path) {
        setInputByPath(path);
        firstPart = NOT_CALCULATED;
        secondPart = NOT_CALCULATED;
    }

    @Override
    public String firstPart() {
        return "First part: " + firstPart;
    }

    @Override
    public String secondPart() {
        return "Second part: " + secondPart;
    }

    @Override
    public void calculate() {
        throw new NotImplementedException();
    }

    @Override
    public void setInputByPath(String filename) {
        input = new AdventOfCodeFileReader(filename).getFileContent();
    }

    @Override
    public void setInput(String input) {
        this.input.clear();
        this.input.addAll(Arrays.asList(input.split("\n")));
    }

    protected void clearData(){
        firstPart = "";
        secondPart = "";
    }
}
