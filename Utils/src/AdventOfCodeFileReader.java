import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AdventOfCodeFileReader {
    private FileInputStream fstream;
    private BufferedReader br;

    public AdventOfCodeFileReader(String filename) {
        try {
            fstream = new FileInputStream("resources/" + filename);
            br = new BufferedReader(new InputStreamReader(fstream));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public List<String> getFileContent() {
        String strLine;
        ArrayList<String> content = new ArrayList<>();

        try {
            while ((strLine = br.readLine()) != null) {
                content.add(strLine);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }
}
