public interface AdventOfCode {

    String firstPart();

    String secondPart();

    void calculate();

    void setInputByPath(String filename);

    void setInput(String input);
}
