import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AdventOfCode03 extends AbstractAdventOfCode {

    public AdventOfCode03() {
        super("AdventOfCode#03");
    }

    @Override
    public void calculate() {
        clearData();
        List<Triangle> triangles = input.stream()
                .map(Triangle::new)
                .collect(Collectors.toList());
        calculateAndSetFirstPart(triangles);
        calculateAndSetSecondPart(triangles);
    }

    private void calculateAndSetFirstPart(List<Triangle> triangles) {
        firstPart = String.valueOf(triangles.stream().filter(Triangle::isTriangle).count());
    }

    private void calculateAndSetSecondPart(List<Triangle> triangles) {
        final List<Integer> firstRow = new ArrayList<>();
        final List<Integer> secondRow = new ArrayList<>();
        final List<Integer> thirdRow = new ArrayList<>();

        triangles.stream().forEach(triangle -> {
            firstRow.add(triangle.getFirstValue());
            secondRow.add(triangle.getSecondValue());
            thirdRow.add(triangle.getThirdValue());
        });

        List<Triangle> newTriangles = new ArrayList<>();

        IntStream.range(0, firstRow.size()).filter(i -> i % 3 == 0).forEach(i -> {
            newTriangles.add(new Triangle(firstRow.get(i), firstRow.get(i + 1), firstRow.get(i + 2)));
            newTriangles.add(new Triangle(secondRow.get(i), secondRow.get(i + 1), secondRow.get(i + 2)));
            newTriangles.add(new Triangle(thirdRow.get(i++), thirdRow.get(i++), thirdRow.get(i)));
        });

        secondPart = String.valueOf(newTriangles.stream().filter(Triangle::isTriangle).count());
    }

}
