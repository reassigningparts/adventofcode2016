public class Main {

    public static void main(String[] args) {
        AdventOfCode AoC = new AdventOfCode03();
        AoC.calculate();
        System.out.println(AoC.firstPart());
        System.out.println(AoC.secondPart());
    }
}
