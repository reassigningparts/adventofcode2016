import java.util.ArrayList;
import java.util.List;

public class Santa {

    private Direction direction = Direction.NORTH;
    private final PointXY thisPosition = new PointXY();
    private final List<PointXY> visitedLocations = new ArrayList<>();
    private boolean firstTimeLost = false;

    public Santa() {
    }

    public void turnLeft() {
        direction = direction.turnLeft();
    }

    public void turnRight() {
        direction = direction.turnRight();
    }

    public void moveForward() {
        thisPosition.move(1, direction);
    }

    public boolean amILostForTheFirstTime() {
        return !firstTimeLost && visitedLocations.stream().filter(location -> location.equals(thisPosition)).findFirst().isPresent();
    }

    public String getDistanceFromDropZone() {
        return "" + thisPosition.getDistanceFromZeroPosition();
    }

    public void drawMap() {
        visitedLocations.add(new PointXY(thisPosition));
    }

    public void recordToFireElfResponsibleForMap() {
        firstTimeLost = true;
    }
}
