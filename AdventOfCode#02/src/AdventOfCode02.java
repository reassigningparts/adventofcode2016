import java.util.stream.IntStream;

public class AdventOfCode02 extends AbstractAdventOfCode {

    public AdventOfCode02() {
        super("AdventOfCode#02");
    }

    @Override
    public void calculate() {
        clearData();
        final Keypad keypadSquare = new Keypad(KeypadType.SQUARE);
        final Keypad keypadDiamond = new Keypad(KeypadType.DIAMOND);

        input.forEach(s -> {
            IntStream.range(0, s.toCharArray().length).forEach(i -> {
                char c = s.charAt(i);
                keypadSquare.moveFinger(c);
                keypadDiamond.moveFinger(c);
            });
            firstPart += keypadSquare.getNumber();
            secondPart += keypadDiamond.getNumber();
        });
    }

}
